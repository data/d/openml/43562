# OpenML dataset: Heart-Disease-patients

https://www.openml.org/d/43562

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
There are many industries where understanding how things group together is beneficial. For example, retailers want to understand the similarities among their customers to direct advertisement campaigns, and botanists classify plants based on their shared similar characteristics. One way to group objects is to use clustering algorithms. We are going to explore the usefulness of unsupervised clustering algorithms to help doctors understand which treatments might work with their patients.
Content
We are going to cluster anonymized data of patients who have been diagnosed with heart disease. Patients with similar characteristics might respond to the same treatments, and doctors could benefit from learning about the treatment outcomes of patients like those they are treating. The data we are analyzing comes from the V.A. Medical Center in Long Beach, CA. To download the data, visit here.
Before running any analysis, it is essential to get an idea of what the data look like. The clustering algorithms we will use require numeric datawe'll check that all the data are numeric.
Acknowledgements
DataCamp

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43562) of an [OpenML dataset](https://www.openml.org/d/43562). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43562/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43562/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43562/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

